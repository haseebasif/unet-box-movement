﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.PlayerConnection;

public class Player_Color : NetworkBehaviour
{

    // Use this for initialization
    public override void OnStartLocalPlayer()
    {
        gameObject.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        if(!isLocalPlayer)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }



}
