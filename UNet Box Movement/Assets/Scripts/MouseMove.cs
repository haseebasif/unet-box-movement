﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.PlayerConnection;

public class MouseMove : NetworkBehaviour
{
    bool down;
    Vector3 mousePos;
    GameObject selectedObject= null;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            down = true;

            mousePos = Input.mousePosition;
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            if (Physics.Raycast(ray, out hit, 100))
            {
                selectedObject = hit.transform.gameObject;

                if (selectedObject.CompareTag("pickup") && isServer && down)
                {
                    //Cmd_RemoveLocalAuthority(selectedObject);
                   // CmdRemoveAuthority(selectedObject.GetComponent<NetworkIdentity>(), this.GetComponent<NetworkIdentity>());
                }
            }
            else
            {
                selectedObject = null;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        { 
            down = false;

            if (selectedObject != null && selectedObject.CompareTag("pickup") && isServer && !down)
            {
                //Cmd_AssignLocalAuthority(selectedObject);
               // CmdSetAuthority(selectedObject.GetComponent<NetworkIdentity>(), this.GetComponent<NetworkIdentity>());
            }
        }

        if ( down && selectedObject != null )
        {
            mousePos = Input.mousePosition;
            mousePos.z = 10;
            selectedObject.transform.position = Camera.main.ScreenToWorldPoint(mousePos);
        }
    }

    [Command]
    void Cmd_AssignLocalAuthority(GameObject obj)
    {
        print("shifting authority successfully");
        NetworkInstanceId nIns = obj.GetComponent<NetworkIdentity>().netId;
        GameObject client = NetworkServer.FindLocalObject(nIns);
        NetworkIdentity ni = client.GetComponent<NetworkIdentity>();
        ni.AssignClientAuthority(connectionToClient);
    }

    [Command]
    void Cmd_RemoveLocalAuthority(GameObject obj)
    {
        print("reverting authority successfully");
        NetworkInstanceId nIns = obj.GetComponent<NetworkIdentity>().netId;
        GameObject client = NetworkServer.FindLocalObject(nIns);
        NetworkIdentity ni = client.GetComponent<NetworkIdentity>();
        ni.RemoveClientAuthority(ni.clientAuthorityOwner);
    }

    //[Command]
    //void CmdSetAuthority(NetworkIdentity grabID, NetworkIdentity playerID)
    //{
    //    grabID.AssignClientAuthority(connectionToClient);
    //}

    //[Command]
    //void CmdRemoveAuthority(NetworkIdentity grabID, NetworkIdentity playerID)
    //{
    //    grabID.RemoveClientAuthority(connectionToClient);
    //}

}