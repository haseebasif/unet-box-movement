﻿using UnityEngine;
using UnityEngine.Networking;

public class PickUpSpawner : NetworkBehaviour
{

    public GameObject PickUpPrefab;

    public override void OnStartServer()
    {
            var PickUp = (GameObject)Instantiate(PickUpPrefab, new Vector3(0,0,0), Quaternion.identity);
            NetworkServer.Spawn(PickUp);
    }
}